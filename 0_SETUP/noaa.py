import requests
from os import path
from time import sleep
from pprint import pprint


class NOAA(object):

    BASE_URL = "https://www.ncdc.noaa.gov/cdo-web/api/v2/"
    _token = "dwGBPEpqJZkaGIoliQzBGAYoHujCPYeZ"

    # Dataset IDs
    WEATHER2 = "NEXRAD2"
    WEATHER3 = "NEXRAD3"
    MONTHLY_NORMALS = "NORMAL_MLY"
    HOURLY_PRECIPITATION = "NORMAL_MLY"


    def get(self, uri):
        res = requests.get(uri, headers={"token": self._token})
        
        if res.status_code == 200:
            return res.json()
        else:
            raise Exception(res.text)


    def data(self, dataset, start, end, location=None, datatype=None):
        base = "%sdata?limit=1000&datasetid=%s&startdate=%s&enddate=%s" % (self.BASE_URL, dataset, start, end)

        if location:
            base += "&locationid=" + location

        """
        if datatype:
            base += '&datatypeid=' + datatype
        """

        data = []
        hasNext = True
        uri = base
        offset = 1
        attempts = 0
        while hasNext:
            if attempts > 9:
                hasNext = False
                break

            try:
                res = self.get(uri)

                if 'results' in res:
                    data += res['results']

                    if res['metadata']['resultset']['count'] > len(data):
                        attempts = 0
                        offset += 1
                        uri = "%s&offset=%d" % (base, offset * 1000)
                    else:
                        hasNext = False
                else:
                    hasNext = False
            except Exception as e:
                pprint(e)
                attempts += 1
                sleep(5)

        return data


    def locations(self):
       return self.get('%slocations?locationcategoryid=ST&limit=52' % self.BASE_URL)['results']

    
    def stations(self, location, dataset):
       return self.get('%sstations?limit=1000&locationid=%s&datasetid=%s' % (self.BASE_URL, location, dataset))['results']
