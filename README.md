# Climate.Park.Change 
## Data Collection and Analysis


#### Requirements

- Docker
- Docker Compose


### Run

```
docker-compose up
```

##### To only fetch your data once ...
Edit the `RUN_SETUP` variable to `'false'` in the _docker-compose.yml_'s environment block.
