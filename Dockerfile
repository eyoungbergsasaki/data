FROM python:3.7

WORKDIR /opt
VOLUME /opt

RUN set -ex \
    ; \
    apt-get update -qq \
    && apt-get upgrade -y \
    ; \
    pip install \
        requests \
        scipy

ENTRYPOINT ["/opt/run"]
